#!/bin/sh

cd optee_os
../build_optee.sh
cd ..

cd u-boot
../build_uboot.sh
cd ..

cd trusted-firmware-a
../build_atf.sh
cd ..

cd u-boot
../build_flashbin.sh
cd ..

FLASHBIN=`find /tmp/uboot-imx8 -name flash.bin`

echo "flash.bin:" "$FLASHBIN"
echo "capsule: " "/tmp/uboot-imx8/capsule1.bin"
