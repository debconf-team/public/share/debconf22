#!/bin/sh

git clone https://github.com/OP-TEE/optee_os.git
git -C optee_os checkout master

git clone https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git
git -C trusted-firmware-a checkout master

git clone https://github.com/ARMmbed/mbedtls.git
git -C mbedtls checkout development
git -C mbedtls checkout 068a13d909ec08a12a5f74289b18142d27977044 -b test1

git clone git://git.denx.de/u-boot.git

git -C u-boot checkout master
